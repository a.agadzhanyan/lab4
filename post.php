<?php
header('Content-Type: text/html; charset=UTF-8');
//
//$errors = FALSE;
//if (empty($_POST['name'])) {
//    print('Заполните имя.<br/>');
//    $errors = TRUE;
//}
//if (empty($_POST['email'])) {
//    print('Заполните email.<br/>');
//    $errors = TRUE;
//}
//if (empty($_POST['yearOfBirth'])) {
//    print('Заполните дату рождения.<br/>');
//    $errors = TRUE;
//}
//if (empty($_POST['biography'])) {
//    print('Заполните о Вас.<br/>');
//    $errors = TRUE;
//}
//if ($_POST['infocheck']!=1) {
//    print('Подтвердите отправку.<br/>');
//    $errors = TRUE;
//}
//
//if ($errors) {
//    exit();
//}
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
    $messages = array();
    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 100000);
        // Если есть параметр save, то выводим сообщение пользователю.
        $messages[0] = 'Спасибо, результаты сохранены.';
    }
    $errors = array();
    $errors['name'] = !empty($_COOKIE['name_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['yearOfBirth'] = !empty($_COOKIE['yearOfBirth_error']);
    $errors['sex'] = !empty($_COOKIE['sex']);
    $errors['countLimbs'] = !empty($_COOKIE['countLimbs_error']);
    $errors['superpower'] = !empty($_COOKIE['superpower_error']);
    $errors['biography'] = !empty($_COOKIE['biography_error']);
    $errors['infocheck'] = !empty($_COOKIE['infocheck_error']);

    // Выдаем сообщения об ошибках.
    if ($errors['name']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('name_error', '', 100000);
        // Выводим сообщение.
        $messages[1] = '<div class="error">Имя не соответствует формату [A-Za-z]</div>';
    }
    if ($errors['email']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('email_error', '', 100000);
        // Выводим сообщение.
        $messages[2] = '<div class="error">Почта не соответствует формату *@mail.ru</div>';
    }
    if ($errors['biography']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('biography_error', '', 100000);
        // Выводим сообщение.
        $messages[3] = '<div class="error">Биография не соответствует формату A-Za-z.-,!</div>';
    }
    if ($errors['yearOfBirth']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('yearOfBirth_error', '', 100000);
        // Выводим сообщение.
        $messages[4] = '<div class="error">Введите дату рождения</div>';
    }
    if ($errors['sex']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('sex_error', '', 100000);
        // Выводим сообщение.
        $messages[5] = '<div class="error">Выберите пол/div>';
    }
    if ($errors['countLimbs']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('countLimbs_error', '', 100000);
        // Выводим сообщение.
        $messages[6] = '<div class="error">Выберите количество конечностей</div>';
    }
    if ($errors['superpower']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('superpower_error', '', 100000);
        // Выводим сообщение.
        $messages[7] = '<div class="error">Выберите супер способности</div>';
    }
    if ($errors['infocheck']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('infocheck_error', '', 100000);
        // Выводим сообщение.
        $messages[8] = '<div class="error">Необходимо согласие</div>';
    }

    // Складываем предыдущие значения полей в массив, если есть.
    $values = array();
    $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
    $values['yearOfBirth'] = empty($_COOKIE['yearOfBirth_value']) ? '' : $_COOKIE['yearOfBirth_value'];
    $values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
    $values['countLimbs'] = empty($_COOKIE['countLimbs_value']) ? '' : $_COOKIE['countLimbs_value'];
    $values['infocheck'] = empty($_COOKIE['infocheck_value']) ? '' : $_COOKIE['infocheck_value'];
    // создаем массив способностей
    $ability = array();
    $ability = empty($_COOKIE['superpower_values']) ? array() : unserialize($_COOKIE['superpower_values'], ["allowed_classes" => false]);
    // unserialize принимает одну сериализованную переменную и конвертирует её обратно в значение PHP.
    // allowed_classes - Массив имён классов, которые должны быть приняты, false для указания не принимать никаких классов

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('index.php');
}
else {
    // Проверяем ошибки.
    $errors = FALSE;
    if (empty($_POST['name']) || preg_match('/^[a-zA-Z]+$/', $_POST['name']) == 0) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        $errors = TRUE;
        setcookie('name_error', '1', time() + 24 * 60 * 60);
    }
    else {
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60 * 365);
    }

    if (empty($_POST['email']) || preg_match("/@mail.ru/", $_POST['email']) == 0) {
        // Выдаем куку на день с флажком об ошибке в поле email.
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60 * 365);
    }

    if (empty($_POST['biography']) || preg_match('/^[a-zA-Z.,!-]+$/' , $_POST['biography']) == 0) {
        // Выдаем куку на день с флажком об ошибке в поле biography.
        setcookie('biography_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60 * 365);
    }

    if (!isset($_POST['sex'])) {
        // Выдаем куку на день с флажком об ошибке в поле biography.
        setcookie('sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60 * 365);
    }

    if (empty($_POST['yearOfBirth']) || preg_match('/^[0-9]+$/' , $_POST['yearOfBirth']) == 0 || ($_POST['yearOfBirth']>2021 || $_POST['yearOfBirth']<1900)) {
        // Выдаем куку на день с флажком об ошибке в поле biography.
        setcookie('yearOfBirth_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('yearOfBirth_value', $_POST['yearOfBirth'], time() + 30 * 24 * 60 * 60 * 365);
    }

    if (!isset($_POST['countLimbs'])) {
        // Выдаем куку на день с флажком об ошибке в поле biography.
        setcookie('countLimbs_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('countLimbs_value', $_POST['countLimbs'], time() + 30 * 24 * 60 * 60 * 365);
    }

    if (!isset($_POST['superpowers'])) {
        // Выдаем куку на день с флажком об ошибке в поле biography.
        setcookie('superpower_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('superpower_values', serialize($_POST['superpowers']), time() + 30 * 24 * 60 * 60 * 365);
    }

    if (!isset($_POST['infocheck'])) {
        // Выдаем куку на день с флажком об ошибке в поле biography.
        setcookie('infocheck_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }




    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: post.php');
        exit();
    }
    else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('name_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('biography_error', '', 100000);
        setcookie('yearOfBirth_error', '', 100000);
        setcookie('sex_error', '', 100000);
        setcookie('countLimbs_error', '', 100000);
        setcookie('superpower_error', '', 100000);
        setcookie('infocheck_error', '', 100000);



    }

    // отправляем данные
    $user = 'u20420';
    $pass = '8832058';
    $db = new PDO('mysql:host=localhost;dbname=u20420', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    // Подготовленный запрос. Не именованные метки.
    try {
        $str = implode(',',$_POST['superpowers']);

        $stmt = $db->prepare("INSERT INTO app SET name = ?, email = ?, yearOfBirth = ?, sex = ?, countLimbs = ?, biograghy = ?");
        $stmt -> execute([$_POST['name'],$_POST['email'],$_POST['yearOfBirth'],$_POST['sex'],$_POST['countLimbs'],$_POST['biography']]);

        $stmt = $db->prepare("INSERT INTO superpowers SET name = ?");
        $stmt -> execute([$str]);

    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }


    // Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');

    // Делаем перенаправление.
    header('Location: post.php');
}

// Сохранение в базу данных.
//
//
//
//$servername = "127.0.0.1";
//$username = "u20420";
//$password = "8832058";
//$dbname = "u20420";
//
////Create connection
//$conn = new mysqli($servername, $username, $password, $dbname);
////Check connection
//if ($conn->connect_error) {
//    die("Connection failed: " . $conn->connect_error);
//}
//
//$sql = "INSERT INTO app (name, email, yearOfBirth, sex, countLimbs, biography, infocheck)
// VALUES ('$name', '$email', '$yearOfBirth', '$sex', '$countLimbs', '$biography', '$infoCheck')";
//
//
//
//if ($conn->query($sql) === TRUE) {
//    echo "New record create successfully";
//} else {
//    echo "Error: " . $sql . "<br>" . $conn->error;
//}
//
//$conn->close();
?>
