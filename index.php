<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        .error {
            border: 2px solid red;
        }
    </style>
</head>
<body>

<div class="container order-1 order-md-2">
    <div class="overflow-auto">
        <div class="form">
            <h1>Формы</h1>
            <form action="" method="POST">
                <div class="form-group row">
                    <?php if ($errors['name']) {print $messages[1];} ?>
                    <label for="inputName" class="col-sm-2 col-form-label">Имя</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" id="inputName" <?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <?php if ($errors['email']) {print $messages[2];} ?>
                    <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" name="email" class="form-control" id="inputEmail" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <?php if ($errors['yearOfBirth']) {print $messages[4];} ?>
                    <label for="inputDate" class="col-sm-2 col-form-label">Дата рождения</label>
                    <div class="col-sm-10">
                        <input type="date" name="yearOfBirth" class="form-control" id="inputDate" <?php if ($errors['yearOfBirth']) {print 'class="error"';} ?> value="<?php print $values['yearOfBirth']; ?>">
                    </div>
                </div>
                <fieldset class="form-group">
                    <div class="row">
                        <?php if ($errors['sex']) {print $messages[5];} ?>
                        <legend class="col-form-label col-sm-2 pt-0">Пол</legend>
                        <div class="col-sm-10">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="sex" id="gridGender1" value="female" checked>
                                <label class="form-check-label" for="gridGender1" <?php if($values['sex']=='Female') print "checked";?>>
                                    Женский
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="sex" id="gridGender2" value="male">
                                <label class="form-check-label" for="gridGender2" <?php if($values['sex']=='Male') print "checked";?>>
                                    Мужской
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="form-group">
                    <div class="row">
                        <?php if ($errors['countLimbs']) {print $messages[6];} ?>
                        <legend class="col-form-label col-sm-2 pt-0">Количество конечностей</legend>
                        <div class="col-sm-10">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="countLimbs" id="gridRadios1" value="allHuman" <?php if($values['countLimbs']=='allHuman') print "checked";?>>
                                <label class="form-check-label" for="gridRadios1">Полный человеческий комплект</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="countLimbs" id="gridRadios2" value="someAlien" <?php if($values['countLimbs']=='someAlien') print "checked";?>>
                                <label class="form-check-label" for="gridRadios2">Немного инопланетянин</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="countLimbs" id="gridRadios3" value="goodForLive" <?php if($values['countLimbs']=='goodForLive') print "checked";?>>
                                <label class="form-check-label" for="gridRadios3">Для жизни хватает</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="countLimbs" id="gridRadios4" value="wantManyHand" <?php if($values['countLimbs']=='wantManyHand') print "checked";?>>
                                <label class="form-check-label" for="gridRadios4">Найти бы еще парочку рук</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="countLimbs" id="gridRadios5" value="infinity" <?php if($values['countLimbs']=='infinity') print "checked";?>>
                                <label class="form-check-label" for="gridRadios5">Бесконечностей</label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="form-group">
                    <div class="row">
                        <?php if ($errors['superpower']) {print $messages[7];} ?>
                        <legend class="col-form-label col-sm-2 pt-0">Сверхспособности</legend>
                        <div class="col-sm-10">
                            <select multiple class="form-control" id="exampleFormControlSelect2" name="superpowers[]" <?php if ($errors['superpower']) {print 'class="error"';} ?>>
                                <option value ="0" <?php if(in_array("0", $superpower)) print "selected";?>>Кодить без багов</option>
                                <option value="1" <?php if(in_array("1", $superpower)) print "selected";?>>Читать мысли животных</option>
                                <option value="2" <?php if(in_array("2", $superpower)) print "selected";?>>Не спать(мечта студента =))</option>
                            </select>
                        </div>
                    </div>
                </fieldset>
                <div class="form-group row">
                    <?php if ($errors['biography']) {print $messages[3];} ?>
                    <label for="inputBiography" class="col-sm-2 col-form-label">Коротко о Вас</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="biography" <?php if ($errors['biography']) {print 'class="error"';} ?>><?php print $values['biography'];?>Меня зовут...</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <?php if ($errors['infocheck']) {print $messages[8];} ?>
                    <div class="col-sm-6">Согласие на обработку персональных данных</div>
                    <div class="col-sm-6">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheck1" name="infoCheck" value="1" <?php if ($errors['infocheck']) {print 'class="error"';} ?>>
                            <label class="form-check-label" for="gridCheck1">Я согласен</label>
                        </div>
                    </div>
                </div>
<!--                <div class="form-group">-->
<!--                    <label for="exampleFormControlFile1">Загрузите файл</label>-->
<!--                    <input type="file" class="form-control-file" id="exampleFormControlFile1">-->
<!--                </div>-->
                <button type="submit">Отправить</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>